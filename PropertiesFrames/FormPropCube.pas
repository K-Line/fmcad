unit FormPropCube;

interface
 uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  KLN.IntfScene,KLN.IntfObject,KLN.Types, KLN.IntfShapes,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, Radiant.Shapes, FMX.Colors, FMX.Edit, FMX.Controls.Presentation,
  FMX.Layouts, FMX.EditBox, FMX.NumberBox;

type
  TFramePropCube = class(TFrame)
    gpCube: TGridPanelLayout;
    lblDimensions: TLabel;
    gpCubeDimensions: TGridPanelLayout;
    nbDimX: TNumberBox;
    nbDimY: TNumberBox;
    nbDimZ: TNumberBox;
    lblCubeLabel: TLabel;
    procedure nbDimXChange(Sender: TObject);
  private
  var
    fObject:IKLNCube;
    fScene:IKLNScene;
    fUserChanges:boolean;
    fNotifyChanges:TProc;
  public
    { Public declarations }
    procedure SetCurrentObject(AObject:TKLNObject); virtual;
    constructor Create(AOwner:TComponent;AScene:IKLNScene;ANotifyChanges:TProc);
  end;


implementation
  uses System.Math.Vectors;

{$R *.fmx}
constructor TFramePropCube.Create(AOwner: TComponent; AScene: IKLNScene;ANotifyChanges:TProc);
begin
  //Initialization
  fUserChanges:=false;
  inherited Create(AOwner);
  fScene:=AScene;
  fUserChanges:=true;
  fNotifyChanges:=ANotifyChanges;
end;

procedure TFramePropCube.nbDimXChange(Sender: TObject);
begin
  inherited;
  if not fUserChanges then  exit;
  //Set Position
  fObject.Dimension:=TPoint3D.Create(nbDimX.Value,nbDimY.Value,nbDimZ.Value);
  if Assigned(fNotifyChanges) then
    fNotifyChanges;
end;

procedure TFramePropCube.SetCurrentObject(AObject: TKLNObject);
begin
  inherited;
  fUserChanges := false;
  //Set current object
 // fObject:=IInterface(AObject);
  //IInterface(AObject).QueryInterface(IKLNCube, fObject);
  //Dimensions
  nbDimX.Value := fObject.Dimension.X;
  nbDimY.Value := fObject.Dimension.Y;
  nbDimZ.Value := fObject.Dimension.Z;

  fUserChanges := true;
end;

end.
