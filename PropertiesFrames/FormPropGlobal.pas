unit FormPropGlobal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  KLN.IntfScene,KLN.IntfObject,KLN.Types,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, Radiant.Shapes, FMX.Colors, FMX.Edit, FMX.Controls.Presentation,
  FMX.Layouts, FMX.EditBox, FMX.NumberBox;

type
  TProc=reference to procedure;
  TFramePropGlobal = class(TFrame)
    glProperties: TGridPanelLayout;
    Label1: TLabel;
    txName: TEdit;
    Label2: TLabel;
    cbColor: TComboColorBox;
    Label3: TLabel;
    GridPanelLayout1: TGridPanelLayout;
    Label4: TLabel;
    GridPanelLayout2: TGridPanelLayout;
    Label5: TLabel;
    GridPanelLayout3: TGridPanelLayout;
    nbPosX: TNumberBox;
    nbRotA: TNumberBox;
    nbRotCenterX: TNumberBox;
    nbPosY: TNumberBox;
    nbRotB: TNumberBox;
    nbPosZ: TNumberBox;
    nbRotC: TNumberBox;
    nbRotCenterY: TNumberBox;
    nbRotCenterZ: TNumberBox;
    procedure cbColorChange(Sender: TObject);
    procedure nbPosXChange(Sender: TObject);
    procedure nbRotAChange(Sender: TObject);
    procedure nbRotCenterXChange(Sender: TObject);
    procedure txNameTyping(Sender: TObject);
  protected
    { Private declarations }
    fObject:TKLNObject;
    fScene:IKLNScene;
    fUserChanges:boolean;
    fNotifyChanges:TProc;
  public
    { Public declarations }
    procedure SetCurrentObject(AObject:TKLNObject); virtual;
    constructor Create(AOwner:TComponent;AScene:IKLNScene;ANotifyChanges:TProc);
  end;

implementation
  uses System.Math.Vectors;

{$R *.fmx}

{ TFramePropGlobal }

procedure TFramePropGlobal.cbColorChange(Sender: TObject);
begin
  if not fUserChanges then  exit;
  //Set Color
  fObject.Color:=cbColor.Color;
end;

constructor TFramePropGlobal.Create(AOwner: TComponent; AScene: IKLNScene;ANotifyChanges:TProc);
begin
  //Initialization
  fUserChanges:=false;
  inherited Create(AOwner);
  fScene:=AScene;
  fUserChanges:=true;
  fNotifyChanges:=ANotifyChanges;
end;

procedure TFramePropGlobal.txNameTyping(Sender: TObject);
begin
  if not fUserChanges then  exit;
  //Set Name
  fObject.Name:=txName.Text;
  if Assigned(fNotifyChanges) then
    fNotifyChanges;
end;

procedure TFramePropGlobal.nbPosXChange(Sender: TObject);
begin
  if not fUserChanges then  exit;
  //Set Position
  fObject.Position:=TPoint3D.Create(nbPosX.Value,nbPosY.Value,nbPosZ.Value);
  if Assigned(fNotifyChanges) then
    fNotifyChanges;
end;

procedure TFramePropGlobal.nbRotAChange(Sender: TObject);
begin
  if not fUserChanges then  exit;
  //Set Rotation
  fObject.Rotation:=TPoint3D.Create(nbRotA.Value,nbRotB.Value,nbRotC.Value);
  if Assigned(fNotifyChanges) then
    fNotifyChanges;
end;

procedure TFramePropGlobal.nbRotCenterXChange(Sender: TObject);
begin
  if not fUserChanges then  exit;
  //Set Rotation
  fObject.RotationCenter:=TPoint3D.Create(nbRotCenterX.Value,nbRotCenterY.Value,nbRotCenterZ.Value);
  if Assigned(fNotifyChanges) then
    fNotifyChanges;
end;

procedure TFramePropGlobal.SetCurrentObject(AObject: TKLNObject);
begin
  fUserChanges:=false;
  //Set current object
  fObject:=AObject;
  //Fill Information
  //Name
  txName.Text:=fObject.Name;
  //Color
  cbColor.Color:=fObject.Color;
  //Position
  nbPosX.Value:=fObject.Position.X;
  nbPosY.Value:=fObject.Position.Y;
  nbPosZ.Value:=fObject.Position.Z;
  //Rotation
  nbRotA.Value:=fObject.Rotation.X;
  nbRotB.Value:=fObject.Rotation.Y;
  nbRotC.Value:=fObject.Rotation.Z;
  //Rotation Center
  nbRotCenterX.Value:=fObject.RotationCenter.X;
  nbRotCenterY.Value:=fObject.RotationCenter.Y;
  nbRotCenterZ.Value:=fObject.RotationCenter.Z;

  fUserChanges:=true;
end;

end.
