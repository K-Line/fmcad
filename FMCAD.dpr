program FMCAD;

uses
  System.StartUpCopy,
  FMX.Forms,
  FormMain in 'FormMain.pas' {FrmMain},
  KLN.IntfScene in 'Interfaces\KLN.IntfScene.pas',
  KLN.FMXScene in 'FMX_3D\KLN.FMXScene.pas',
  KLN.IntfObject in 'Interfaces\Shapes\KLN.IntfObject.pas',
  KLN.Types in 'KLN.Types.pas',
  KLN.FMXCube in 'FMX_3D\Shapes\KLN.FMXCube.pas',
  KLN.FMXObject in 'FMX_3D\Shapes\KLN.FMXObject.pas',
  FormPropGlobal in 'PropertiesFrames\FormPropGlobal.pas' {FramePropGlobal: TFrame},
  KLN.FMXPoint in 'FMX_3D\Shapes\KLN.FMXPoint.pas',
  KLN.IntfShapes in 'Interfaces\Shapes\KLN.IntfShapes.pas',
  KLN.FMXSphere in 'FMX_3D\Shapes\KLN.FMXSphere.pas',
  KLN.FMXCone in 'FMX_3D\Shapes\KLN.FMXCone.pas',
  KLN.FMXCylinder in 'FMX_3D\Shapes\KLN.FMXCylinder.pas',
  FormPropCube in 'PropertiesFrames\FormPropCube.pas' {FramePropCube: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;
end.
