unit KLN.FMXPoint;

interface
uses
  system.classes,
  KLN.FMXObject,KLN.Types,
  FMX.Objects,FMX.Objects3D,
  System.Math.Vectors,System.UITypes;

type
  TKLNFMXPoint=class(TKLNFMXObject)
  private
    function Create3D():Boolean; override;
  end;

implementation

{ TFMXCube }

function TKLNFMXPoint.Create3D: Boolean;
begin
  fName:='Point';
  fShape:=TShape3D.Create(nil);
  fShape.HitTest:=false;
  fShape.Visible:=true;
  fShape.Position.Point:=TPoint3D.Zero;
end;

end.
