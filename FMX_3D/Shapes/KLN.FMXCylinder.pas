unit KLN.FMXCylinder;

interface
uses
  system.classes,
  KLN.FMXObject,KLN.Types,KLN.IntfShapes,
  FMX.Objects,FMX.Objects3D,KLN.FMXCone,
  System.Math.Vectors,System.UITypes;

type
  TKLNFMXCylinder=class(TKLNFMXCone)
  private
    function Create3D: Boolean; override;
  end;

implementation

function TKLNFMXCylinder.Create3D: Boolean;
var
  fShapeCyl:TCylinder;
begin
  fName:='Cylinder';
  fShapeCyl:=TCylinder.Create(nil);
  fShapeCyl.HitTest:=false;
  fShapeCyl.Visible:=true;
  fShapeCyl.Height:=2;
  fShapeCyl.Width:=2;
  fShapeCyl.Depth:=2;
  fShapeCyl.Position.Point:=TPoint3D.Zero;

  fShape:=fShapeCyl;
end;

end.
