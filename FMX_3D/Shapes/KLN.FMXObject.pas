unit KLN.FMXObject;

interface
  uses KLN.IntfObject,
    System.Math.Vectors,System.UITypes,System.Generics.Collections,
    FMX.Objects3D;
  type
    TKLNFMXObject=class(TKLNObject)
      protected
        fShape:TShape3D;
      public
        //Object Methods
        destructor Destroy();override;
        property Shape:TShape3D read fShape write fShape;
        //Interface methods
        function SetParent(AParent:TKLNObject):Boolean; override;

        procedure SetColor(AValue: TAlphaColor); override;
        function GetColor():TAlphaColor;  override;
        procedure SetPosition(AValue:TPoint3D);override;
        function GetPosition():TPoint3D;  override;
        procedure SetRotation(AValue: TPoint3D);override;
        function GetRotation():TPoint3D;  override;
        procedure SetRotationCenter(AValue: TPoint3D);override;
        function GetRotationCenter():TPoint3D;  override;
    end;

implementation
  uses FMX.Types3D,SysUtils,FMX.MaterialSources;

{ TFMXObject }
function TKLNFMXObject.SetParent(AParent:TKLNObject):Boolean;
begin
  //Removes from current parent
  if fParent<>nil then
    fParent.ChildRemove(Self);
  //Set parent
  if AParent<>nil then
    AParent.ChildAdd(self);

  if AParent<>nil then
    TKLNFMXObject(AParent).Shape.AddObject(fShape);
  fParent:=AParent;
end;

destructor TKLNFMXObject.Destroy;
begin
  inherited;
  fShape.Destroy;
end;

function TKLNFMXObject.GetColor():TAlphaColor;
begin
  result:=TAlphaColorRec.White;
  if fShape.MaterialSource<>nil then
    result:=TColorMaterialSource(fShape.MaterialSource).Color;
end;

procedure TKLNFMXObject.SetColor(AValue: TAlphaColor);
begin
  if fShape.MaterialSource=nil then
    fShape.MaterialSource:=TColorMaterialSource.Create(nil);

  TColorMaterialSource(fShape.MaterialSource).Color:=AValue;
end;

procedure TKLNFMXObject.SetPosition(AValue: TPoint3D);
begin
  fShape.Position.Point:=AValue;
end;

procedure TKLNFMXObject.SetRotation(AValue: TPoint3D);
begin
  fShape.RotationAngle.Point:=AValue;
end;

procedure TKLNFMXObject.SetRotationCenter(AValue: TPoint3D);
begin
  fShape.RotationCenter.Point:=AValue;
end;

function TKLNFMXObject.GetPosition():TPoint3D;
begin
  result:=fShape.Position.Point;
end;

function TKLNFMXObject.GetRotation():TPoint3D;
begin
  result:=fShape.RotationAngle.Point;
end;

function TKLNFMXObject.GetRotationCenter():TPoint3D;
begin
  result:=fShape.RotationCenter.Point;
end;


end.
