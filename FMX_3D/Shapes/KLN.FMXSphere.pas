unit KLN.FMXSphere;

interface
uses
  system.classes,
  KLN.FMXObject,KLN.Types,KLN.IntfShapes,
  FMX.Objects,FMX.Objects3D,
  System.Math.Vectors,System.UITypes;

type
  TKLNFMXSphere=class(TKLNFMXObject,IKLNSphere)
  private
  var
    function Create3D():Boolean; override;
    function GetRadius():Single;
    procedure SetRadius(AValue:Single);
  protected
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
   function _AddRef: Integer; stdcall;
   function _Release: Integer; stdcall;
  end;

implementation

{ TFMXSphere }

function TKLNFMXSphere.Create3D: Boolean;
var
  fShapeSphere:TSphere;
begin
  fName:='Sphere';
  fShapeSphere:=TSphere.Create(nil);
  fShapeSphere.HitTest:=false;
  fShapeSphere.Visible:=true;
  fShapeSphere.Height:=2;
  fShapeSphere.Width:=2;
  fShapeSphere.Depth:=2;
  fShapeSphere.Position.Point:=TPoint3D.Zero;

  fShape:=fShapeSphere;
end;

function TKLNFMXSphere.GetRadius: Single;
begin
  result:= TSphere(fShape).Width/2;
end;

procedure TKLNFMXSphere.SetRadius(AValue: Single);
begin
  TSphere(fShape).Height:=AValue*2;
  TSphere(fShape).Width:=AValue*2;
  TSphere(fShape).Depth:=AValue*2;
end;

function TKLNFMXSphere.QueryInterface(const IID: TGUID; out Obj): HResult;
begin

end;

function TKLNFMXSphere._AddRef: Integer;
begin

end;

function TKLNFMXSphere._Release: Integer;
begin

end;

end.
