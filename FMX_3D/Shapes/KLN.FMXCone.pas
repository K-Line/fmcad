unit KLN.FMXCone;

interface
uses
  system.classes,
  KLN.FMXObject,KLN.Types,KLN.IntfShapes,
  FMX.Objects,FMX.Objects3D,
  System.Math.Vectors,System.UITypes;

type
  TKLNFMXCone=class(TKLNFMXObject,IKLNCone)
  private
  var
    function Create3D():Boolean; override;
    function GetTopRadius():Single;
    procedure SetTopRadius(AValue:Single);

    function GetBottomRadius():Single;
    procedure SetBottomRadius(AValue:Single);

    function GetHeight():Single;
    procedure SetHeight(AValue:Single);
  protected
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
   function _AddRef: Integer; stdcall;
   function _Release: Integer; stdcall;
  end;

implementation

{ TFMXCone }

function TKLNFMXCone.Create3D: Boolean;
var
  fShapeCone:TCone;
begin
  fName:='Cone';
  fShapeCone:=TCone.Create(nil);
  fShapeCone.HitTest:=false;
  fShapeCone.Visible:=true;
  fShapeCone.Height:=2;
  fShapeCone.Width:=2;
  fShapeCone.Depth:=2;
  fShapeCone.Position.Point:=TPoint3D.Zero;

  fShape:=fShapeCone;
end;

function TKLNFMXCone.GetBottomRadius: Single;
begin
  result:=fShape.Width/2;
end;

function TKLNFMXCone.GetHeight: Single;
begin
  result:=fShape.Height;
end;

function TKLNFMXCone.GetTopRadius: Single;
begin
  result:=fShape.Width/2;
end;

procedure TKLNFMXCone.SetBottomRadius(AValue: Single);
begin
  fShape.Width:=AValue*2;
end;

procedure TKLNFMXCone.SetHeight(AValue: Single);
begin
  fShape.Height:=AValue;
end;

procedure TKLNFMXCone.SetTopRadius(AValue: Single);
begin
  fShape.Width:=AValue*2;
end;
function TKLNFMXCone.QueryInterface(const IID: TGUID; out Obj): HResult;
begin

end;


function TKLNFMXCone._AddRef: Integer;
begin

end;

function TKLNFMXCone._Release: Integer;
begin

end;

end.
