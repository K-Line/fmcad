unit KLN.FMXCube;

interface
uses
  system.classes,
  KLN.FMXObject,KLN.Types,KLN.IntfShapes,
  FMX.Objects,FMX.Objects3D,
  System.Math.Vectors,System.UITypes;

type
  TKLNFMXCube=class(TKLNFMXObject,IKLNCube)
  private
  var
    function Create3D():Boolean; override;
    function GetDimensions():TPoint3D;
    procedure SetDimensions(AValue:TPoint3D);
  protected
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
   function _AddRef: Integer; stdcall;
   function _Release: Integer; stdcall;
  end;

implementation

{ TFMXCube }

function TKLNFMXCube.Create3D: Boolean;
var
  fShapeCube:TCube;
begin
  fName:='Cube';
  fShapeCube:=TCube.Create(nil);
  fShapeCube.HitTest:=false;
  fShapeCube.Visible:=true;
  fShapeCube.Height:=2;
  fShapeCube.Width:=2;
  fShapeCube.Depth:=2;
  fShapeCube.Position.Point:=TPoint3D.Zero;

  fShape:=fShapeCube;
end;

function TKLNFMXCube.GetDimensions: TPoint3D;
begin
  result:= TPoint3D.Create(fShape.Width,fShape.Depth,fShape.Height);
end;

procedure TKLNFMXCube.SetDimensions(AValue: TPoint3D);
begin
  fShape.Width:=AValue.X;
  fShape.Depth:=AValue.Y;
  fShape.Height:=AValue.Z;
end;

function TKLNFMXCube.QueryInterface(const IID: TGUID; out Obj): HResult;
begin

end;

function TKLNFMXCube._AddRef: Integer;
begin

end;

function TKLNFMXCube._Release: Integer;
begin

end;

end.
