unit KLN.FMXScene;

interface
uses
  FMX.Viewport3D,FMX.Controls3D, FMX.Types,FMX.Objects3D, FMX.MaterialSources,
  System.Classes,System.UITypes,System.Types, system.generics.collections , System.SysUtils,
  KLN.intfScene,KLN.IntfObject,KLN.Types,KLN.FMXObject;

type
  TFMXScene=class(TInterfacedObject,IKLNScene)
  private
    //Basic Scene Objects
    fDummy:TDummy;
    fViewPort:TViewport3D;
    fCamera:TCamera;
    fLight:TLight;
    fCenter:TStrokeCube;
   // fC:TCube;
    //Camera Events
    fDown:TPointf;
    procedure DoZoom(aIn: Boolean);
    procedure SceneOnMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; var Handled: Boolean);
    procedure SceneOnMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure SceneOnMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
    const
      CAMERA_MAX_Z = -1;
      CAMERA_MIN_Z = -200;
      ZOOM_STEP = 2;
    //Inner Objects
    var
    fMaterials:TObjectList<TColorMaterialSource> ;
    fProject:TKLNFMXObject;
    fSelected:TKLNFMXObject;
  public
    constructor Create(AOwner:TFMXObject);
    destructor Destroy();override;
    //Interface Methods
    Function ObjectCreate(AParent:TKLNObject;AKind:EKLNObjectKind;AColor:TAlphaColor):TKLNObject;
    Function ProjectCreate(AName:string):TKLNObject;
    Procedure ObjectSelect(AObject: TKLNObject; DrillDown: boolean);
    Function ObjectDelete(AObject:TKLNObject):boolean;
  end;

implementation
uses
  System.Math.Vectors,FMX.Types3D,
  KLN.FMXCube,KLN.FMXPoint,KLN.FMXSphere,KLN.FMXCone,KLN.FMXCylinder;

{ TFMXScene }

constructor TFMXScene.Create(AOwner:TFMXObject);
//var
//  fC:TKLNFMXObject;
begin
  //Create Scene
  fViewPort:=TViewport3D.Create(nil);
  fViewPort.Align:= TAlignLayout.Client;
  fViewPort.Parent:=AOwner;
  fViewPort.UsingDesignCamera:=false;
  //Add Camera
  fDummy:=TDummy.Create(nil);
  fViewPort.AddObject(fDummy);
  fCamera:=TCamera.Create(nil);
  fCamera.Projection:=TProjection.Screen;
  fDummy.AddObject(fCamera);
  fViewPort.Camera:=fCamera;
  //Add Light
  fLight:=TLight.Create(nil);
  fViewPort.AddObject(fLight);
  //Add Center
  fCenter:=TStrokeCube.Create(nil);
  fCenter.Color:=TAlphaColorRec.Blue;
  fCenter.HitTest:=false;
  fViewPort.AddObject(fCenter);
  //Add Zoom
  fViewPort.OnMouseWheel:=SceneOnMouseWheel;
  //Add Rotate
  fViewPort.OnMouseDown:=SceneOnMouseDown;
  fViewPort.OnMouseMove:=SceneOnMouseMove;
  //Add Pan
  fCamera.Target:= fCenter;
  {fC:=TKLNFMXCube.Create();
  fC.Shape.HitTest:=false;
  fC.Shape.Visible:=true;
  fC.Shape.Height:=2;
  fC.Shape.Width:=2;
  fC.Shape.Depth:=2;
  fC.Shape.Position.Point:=TPoint3D.Create(4,4,4);
     fC.Shape.Parent:=fViewPort;  }
  //
  fProject:=nil;
  fSelected:=nil;
  fMaterials:=TObjectList<TColorMaterialSource>.Create() ;
end;

destructor TFMXScene.Destroy;
begin
  if fProject<>nil then
    fProject.Destroy;
  fMaterials.Destroy;
  fCenter.Destroy;
  fLight.Destroy;
  fCamera.Destroy;
  fViewPort.Destroy;
  inherited;
end;

Procedure TFMXScene.ObjectSelect(AObject:TKLNObject;DrillDown:boolean);
begin
  //Deselect previous
  if fSelected<>nil then
    fSelected.Shape.Opacity:=1;

  fSelected:=TKLNFMXObject(AObject);
  fSelected.Shape.Opacity:=0.5;
  //Select
end;

function TFMXScene.ProjectCreate(AName: string): TKLNObject;
begin
  fProject:=TKLNFMXPoint.Create(EKLNPoint);
  fProject.Name:=AName;
  fProject.Shape.Parent:=fViewPort;
  fSelected:=fProject;
  result:=fProject;
end;

function TFMXScene.ObjectCreate(AParent:TKLNObject;AKind: EKLNObjectKind;AColor:TAlphaColor): TKLNObject;
var
  cadObj:TKLNFMXObject;
begin
  result:=nil;
  //Set parent
  if AParent=nil then
    if fProject=nil then
      exit
    else
      AParent:=fProject;

  cadObj:=nil;
  //Build 3D Objects
  case AKind of
    EKLNCube:
      cadObj:=TKLNFMXCube.Create(EKLNCube);
    EKLNPoint:
      cadObj:=TKLNFMXPoint.Create(EKLNPoint);
    EKLNSphere:
      cadObj:=TKLNFMXSphere.Create(EKLNSphere);
    EKLNCone:
      cadObj:=TKLNFMXCone.Create(EKLNCone);
    EKLNCylinder:
      cadObj:=TKLNFMXCylinder.Create(EKLNCylinder);
  end;
  //Add to scene
  if cadObj<>nil then
  begin
    //Set Color
    cadObj.Color:=AColor;
    cadObj.SetParent(AParent);
    result:=cadObj;
    fViewPort.Repaint;
  end;
end;

function TFMXScene.ObjectDelete(AObject: TKLNObject): boolean;
begin
  result:=false;
  //Delete Object
  if AObject<>nil then
  begin
    if AObject=fSelected then
      fSelected:=nil;
    AObject.SetParent(nil);
    AObject.Destroy;
    AObject:=nil;
    fViewPort.Repaint;
    result:=true;
  end;

end;

{$REGION 'Scene Events'}

  procedure TFMXScene.SceneOnMouseWheel(Sender: TObject; Shift: TShiftState;
    WheelDelta: Integer; var Handled: Boolean);
  begin
     DoZoom(WheelDelta > 0);
  end;



procedure TFMXScene.DoZoom(aIn: Boolean);
  var
    newZ: Single;
  begin
    if aIn then
      newZ := fCamera.Position.Z + ZOOM_STEP
    else
      newZ := fCamera.Position.Z - ZOOM_STEP;

    if (newZ < CAMERA_MAX_Z) and (newZ > CAMERA_MIN_Z) then
      fCamera.Position.Z := newZ;
  end;


  procedure TFMXScene.SceneOnMouseDown(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Single);
  begin
    FDown:=Pointf(x,y);
  end;

  procedure TFMXScene.SceneOnMouseMove(Sender: TObject; Shift: TShiftState; X,
    Y: Single);
  begin
    if ssleft in shift then
    begin
      fDummy.RotationAngle.X:=      fDummy.RotationAngle.X-((Y-fdown.Y));
      fDummy.RotationAngle.Y:=      fDummy.RotationAngle.Y+((X-fdown.X));
      FDown:=Pointf(x,y);
    end;
  end;

{$ENDREGION}

end.
