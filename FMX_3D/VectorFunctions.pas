unit VectorFunctions;

interface
//THIS IS COPY  FROM THE GLSCENE PROJECT CAMERA
USES  FMX.Controls3D,system.math.vectors,system.math;

procedure RotateObject(obj: Tcamera; pitchDelta, turnDelta:
  Single;
  rollDelta: Single = 0);
function VectorSubtract(V1,V2:Tvector3D):Tvector3D;

implementation

function VectorSubtract(V1,V2:Tvector3D):Tvector3D;
begin
   Result.X:=v1.X-v2.X;
   Result.Y:=v1.Y-v2.Y;
   Result.Z:=v1.Z-v2.Z;
   Result.W:=v1.W-v2.W;
end;

procedure SetVector(out v : Tvector3D; const vSrc : Tvector3D);
begin
   // faster than memcpy, move or ':=' on the TVector...
 	v.X:=vSrc.X;
	v.Y:=vSrc.Y;
	v.Z:=vSrc.Z;
	v.W:=vSrc.W;
end;

function VectorCrossProduct(const v1, v2 : Tvector3D) : Tvector3D;
begin
   Result.X:=v1.Y*v2.Z-v1.Z*v2.Y;
   Result.Y:=v1.Z*v2.X-v1.X*v2.Z;
   Result.Z:=v1.X*v2.Y-v1.Y*v2.X;
   Result.W:=0;

end;

function VectorDotProduct(const V1, V2 : Tvector3D) : Single;
begin
   Result:=V1.X*V2.X+V1.Y*V2.Y+
           V1.Z*V2.Z+V1.W*V2.W;
end;

function ClampValue(const aValue, aMin, aMax : Single) : Single;
begin
   if aValue<aMin then
      Result:=aMin
   else if aValue>aMax then
      Result:=aMax
   else Result:=aValue;
end;
function VectorTransform(const V: Tvector3D; const M: TMatrix3D) : Tvector3D;
begin
  Result.X:=V.X * M.m11 + V.Y * M.m21 + V.Z * M.m31 + V.W * M.m41;
  Result.Y:=V.X * M.m12 + V.Y * M.m22 + V.Z * M.m32 + V.W * M.m42;
  Result.Z:=V.X * M.m13 + V.Y * M.m23 + V.Z * M.m33 + V.W * M.m43;
  Result.W:=V.X * M.m14 + V.Y * M.m24 + V.Z * M.m34 + V.W * M.m44;
end;

procedure SinCosine(const Theta: Double; out Sin, Cos: Single);
var
   s, c : Extended;
begin
   SinCos(Theta, s, c);
   Sin:=s; Cos:=c;
end;

function CreateRotationMatrix(const anAxis : Tvector3D; angle : Single) : TMatrix3D;
var
   axis : Tvector3D;
   cosine, sine,one_minus_cosine : Single;
begin
   SinCosine(angle, sine, cosine);
   one_minus_cosine:=1-cosine;
   axis:=anAxis.Normalize;

   Result.M11:=(one_minus_cosine * axis.X * axis.X) + cosine;
   Result.m12:=(one_minus_cosine * axis.X * axis.Y) - (axis.Z * sine);
   Result.m13:=(one_minus_cosine * axis.Z * axis.X) + (axis.Y * sine);
   Result.m14:=0;

   Result.m21:=(one_minus_cosine * axis.X * axis.Y) + (axis.Z * sine);
   Result.m22:=(one_minus_cosine * axis.Y * axis.Y) + cosine;
   Result.m23:=(one_minus_cosine * axis.Y * axis.Z) - (axis.X * sine);
   Result.m24:=0;

   Result.m31:=(one_minus_cosine * axis.Z * axis.X) - (axis.Y * sine);
   Result.m32:=(one_minus_cosine * axis.Y * axis.Z) + (axis.X * sine);
   Result.m33:=(one_minus_cosine * axis.Z * axis.Z) + cosine;
   Result.m34:=0;

   Result.m41:=0;
   Result.m42:=0;
   Result.m43:=0;
   Result.m44:=1;
end;

procedure ScaleVector(var v : Tvector3D; factor: Single);
begin
   v.x:=v.x*factor;
   v.y:=v.y*factor;
   v.z:=v.z*factor;
   v.w:=v.w*factor;
end;

procedure RotateVector(var vector : Tvector3D; const axis : Tvector3D; angle : Single);
var
   rotMatrix : TMatrix3D;
begin
   rotMatrix:=CreateRotationMatrix(axis, Angle);
   vector:=VectorTransform(vector, rotMatrix);
end;

function VectorAdd(const v1, v2 : Tvector3D) : Tvector3D;
begin
   Result.x:=v1.x+v2.x;
   Result.y:=v1.y+v2.y;
   Result.z:=v1.z+v2.z;
   Result.w:=v1.w+v2.w;
end;


procedure RotateObject(obj: Tcamera; pitchDelta, turnDelta:
  Single;
  rollDelta: Single = 0);
var
  originalT2C, normalT2C, normalCameraRight, newPos: Tvector3D;
  pitchNow, dist: Single;
begin
  if Assigned(obj.Target) then
  begin
    // normalT2C points away from the direction the camera is looking
    originalT2C := VectorSubtract(obj.Target.AbsolutePosition,
      obj.AbsolutePosition);
    SetVector(normalT2C, originalT2C);
    dist := normalT2C.Length;
    normalT2C:=normalT2C.normalize;
    // normalRight points to the camera's right
    // the camera is pitching around this axis.
    normalCameraRight := VectorCrossProduct(obj.target.AbsoluteUp, normalT2C);
    if normalCameraRight.length < 0.001 then
      SetVector(normalCameraRight, TVector3D.create(1,0,0)) // arbitrary vector
    else
      normalCameraRight:=normalCameraRight.normalize;
    // calculate the current pitch.
    // 0 is looking down and PI is looking up
    pitchNow := Arccos(VectorDotProduct(obj.target.AbsoluteUp, normalT2C));
    pitchNow := ClampValue(pitchNow + DegToRad(pitchDelta), 0 + 0.025, PI -
      0.025);
    // create a new vector pointing up and then rotate it down
    // into the new position
    SetVector(normalT2C, obj.target.AbsoluteUp);
    RotateVector(normalT2C, normalCameraRight, -pitchNow);
    RotateVector(normalT2C, obj.target.AbsoluteUp, -DegToRad(turnDelta));
    ScaleVector(normalT2C, dist);
    newPos := VectorAdd(obj.target.AbsolutePosition, VectorSubtract(normalT2C,
      originalT2C));
    obj.target.AbsolutePosition := newPos;
  end;
end;

end.
