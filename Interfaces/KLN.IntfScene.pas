unit KLN.IntfScene;

interface
uses
  System.Classes,KLN.IntfObject,KLN.Types,System.UITypes;

type
    IKLNScene=interface
      ['{E3116442-E262-4B13-8CF2-234B69ED0A7A}']
      Function ObjectCreate(AParent:TKLNObject;AKind:EKLNObjectKind;AColor:TAlphaColor):TKLNObject;
      Function ProjectCreate(AName:string):TKLNObject;
      Procedure ObjectSelect(AObject:TKLNObject;DrillDown:boolean);
      Function ObjectDelete(AObject:TKLNObject):boolean;
    end;

implementation

end.
