unit KLN.IntfShapes;

interface

uses System.Math.Vectors;

type
  IKLNCube = interface
    function GetDimensions():TPoint3D;
    procedure SetDimensions(AValue:TPoint3D);
    property Dimension:TPoint3D read GetDimensions write SetDimensions;
  end;

  IKLNSphere = interface
    function GetRadius():Single;
    procedure SetRadius(AValue:Single);
    property Radius:Single read GetRadius write SetRadius;
  end;

  IKLNCone = interface
    function GetTopRadius():Single;
    procedure SetTopRadius(AValue:Single);
    property TopRadius:Single read GetTopRadius write SetTopRadius;
    //
    function GetBottomRadius():Single;
    procedure SetBottomRadius(AValue:Single);
    property BottomRadius:Single read GetBottomRadius write SetBottomRadius;
    //
    function GetHeight():Single;
    procedure SetHeight(AValue:Single);
    property Height:Single read GetHeight write SetHeight;
  end;

  IKLNMesh = interface

  end;

implementation

end.
