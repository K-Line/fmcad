unit KLN.IntfObject;

interface
uses System.Math.Vectors,System.UITypes,KLN.Types,System.Generics.Collections;
  type

    TKLNObject=class
    protected
      fKind:EKLNObjectKind;
      fID:TGuid;
      fName:string;
      fChilds:TList<TKLNObject>;
      fParent:TKLNObject;
      //Properties Methods
      procedure SetColor(AValue:TAlphaColor); virtual; abstract;
      function GetColor():TAlphaColor; virtual; abstract;
      procedure SetPosition(APosition:TPoint3D); virtual; abstract;
      function GetPosition():TPoint3D; virtual; abstract;
      procedure SetRotation(APosition:TPoint3D); virtual; abstract;
      function GetRotation():TPoint3D; virtual; abstract;
      procedure SetRotationCenter(APosition:TPoint3D); virtual; abstract;
      function GetRotationCenter():TPoint3D; virtual; abstract;

      procedure SetName(AValue:string);
    public
      constructor Create(AKind:EKLNObjectKind);
      destructor Destroy(); override;
      property Kind:EKLNObjectKind read fKind;
      function Create3D():Boolean;  virtual; abstract;
      function ChildAdd(AChild:TKLNObject):Boolean;  virtual;
      function ChildRemove(AChild:TKLNObject):Boolean;  virtual;
      function SetParent(AParent:TKLNObject):Boolean;  virtual; abstract;
      function Refresh3D():Boolean;  virtual; abstract;
 
      //Properties
      property ID:TGuid read fID;
      property Name:String read fName write SetName;
      property Color:TAlphaColor read GetColor write SetColor;
      property Position:TPoint3D read GetPosition write SetPosition;
      property Rotation:TPoint3D read GetRotation write SetRotation;
      property RotationCenter:TPoint3D read GetRotationCenter write SetRotationCenter;
    end;


implementation
  uses sysutils;



{ TKLNObject }

function TKLNObject.ChildAdd(AChild: TKLNObject): Boolean;
begin
  if AChild=nil then
    Exit;

  fChilds.Add(AChild);
  //AChild.SetParent(self);
end;

function TKLNObject.ChildRemove(AChild: TKLNObject): Boolean;
begin
  if AChild=nil then
    Exit;

  fChilds.Remove(AChild);
  //AChild.SetParent(nil);
end;

constructor TKLNObject.Create(AKind: EKLNObjectKind);
begin
  fID:=TGuid.NewGuid;
  fKind:=AKind;
  fName:='Shape';
  fParent:=nil;
  fChilds:=TList<TKLNObject>.create;
  Create3D();
end;

destructor TKLNObject.Destroy;
begin
  while fChilds.Count>0 do
  begin
    fChilds[0].Destroy;
    fChilds.Delete(0);
  end;

  fChilds.Destroy;
  inherited;
end;

procedure TKLNObject.SetName(AValue: string);
begin
  fName:=AValue;
  if fName='' then
    fName:='Shape';
end;

end.
