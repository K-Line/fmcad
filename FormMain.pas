unit FormMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  KLN.IntfScene,KLN.FMXScene,KLN.IntfObject,
  FormPropGlobal,FormPropCube,  KLN.Types,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Viewport3D,
  System.Math.Vectors, FMX.Controls3D, FMX.Types3D, FMX.Objects3D, FMX.StdCtrls,
  FMX.Controls.Presentation, System.ImageList, FMX.ImgList, FMX.TreeView,
  FMX.Layouts, FMX.Colors;

type
  TFrmMain = class(TForm)
    Panel1: TPanel;
    StyleBook1: TStyleBook;
    ImageList1: TImageList;
    Panel3: TPanel;
    btnCube: TButton;
    BtnCone: TButton;
    Button4: TButton;
    btnCylinder: TButton;
    btnSphere: TButton;
    Label1: TLabel;
    Panel4: TPanel;
    Panel2: TPanel;
    Button2: TButton;
    Button9: TButton;
    Button10: TButton;
    Splitter1: TSplitter;
    Label2: TLabel;
    tvItems: TTreeView;
    TreeViewItem1: TTreeViewItem;
    TreeViewItem2: TTreeViewItem;
    Label3: TLabel;
    cbNewShapeColor: TComboColorBox;
    PProps: TPanel;
    btnPoint: TButton;
    btnDelete: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCubeClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure tvItemsChange(Sender: TObject);
    procedure tvItemsDragChange(SourceItem, DestItem: TTreeViewItem;
      var Allow: Boolean);
    procedure tvItemsDragOver(Sender: TObject; const Data: TDragObject;
      const Point: TPointF; var Operation: TDragOperation);
    procedure btnPointClick(Sender: TObject);
    procedure btnSphereClick(Sender: TObject);
    procedure btnCylinderClick(Sender: TObject);
    procedure BtnConeClick(Sender: TObject);
  private
    { Private declarations }
    fScene:IKLNScene;
    fCurProject:TKLNObject;
    fGlobalFrame:TFramePropGlobal;
    fCubeFrame:TFramePropCube;
   // fCubeFrame:TFramePropCube;
    procedure TreeAddItem(AObject: TKLNObject;AAddComponent:boolean=true);
    procedure CreateShape(AKind: EKLNObjectKind);
  public
    { Public declarations }
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.fmx}
{$REGION 'Shape Events'}

procedure TFrmMain.CreateShape(AKind:EKLNObjectKind);
var
  newShape,parentShape:TKLNObject;
begin
  //New point object
  newShape:=fScene.ObjectCreate(nil,AKind,cbNewShapeColor.Color);
  //Add Tree
  if newShape<>nil then
    TreeAddItem(newShape);
end;

procedure TFrmMain.btnCubeClick(Sender: TObject);
begin
  CreateShape(EKLNCube);
end;

procedure TFrmMain.btnCylinderClick(Sender: TObject);
begin
  CreateShape(EKLNCylinder);
end;

procedure TFrmMain.btnDeleteClick(Sender: TObject);
var
  tItem:TTreeViewItem;
begin
  //TODO - MultiSelection
  //ask User
  tItem:=tvItems.Selected;
  if (tItem<>nil) and (tItem.TagObject<>nil) then
    if true then
      if fScene.ObjectDelete(TKLNObject(tItem.TagObject)) then
      begin
        //Select next item
        if tvItems.Count>tItem.Index+1 then
          tvItems.Items[tItem.Index+1].Select
        else
          if tItem.Index>0 then
            tvItems.Items[tItem.Index-1].Select;

        //Remove tree View item
        tvItems.RemoveObject(tItem);
        tItem.Destroy;
      end;
end;

procedure TFrmMain.btnPointClick(Sender: TObject);
begin
  CreateShape(EKLNPoint);
end;

procedure TFrmMain.btnSphereClick(Sender: TObject);
begin
  CreateShape(EKLNSphere);
end;

procedure TFrmMain.BtnConeClick(Sender: TObject);
begin
  CreateShape(EKLNCone);
end;

{$ENDREGION}

{$REGION 'Tree Events'}

  procedure TFrmMain.TreeAddItem(AObject:TKLNObject;AAddComponent:boolean=true);
  var
    treeItem:TTreeViewItem;
  begin
    treeItem:=TTreeViewItem.Create(nil);
    treeItem.Text:=AObject.Name;
    treeItem.TagObject:=AObject;
    if AAddComponent then
      tvItems.Items[0].AddObject(treeItem)
    else
      tvItems.AddObject(treeItem);
    tvItems.Selected:=treeItem;
  end;

  procedure TFrmMain.tvItemsChange(Sender: TObject);
    procedure FillObjectProperties();
    var
      item:TKLNObject;
    begin
      item:=TKLNObject(tvItems.Selected.TagObject);
      fGlobalFrame.Visible:=true;
      //Show Frame by type
      case item.Kind of
        EKLNCube:
        begin
          //fCubeFrame.Visible:=true;
//          fCubeFrame.SetCurrentObject(item);
        end;

        EKLNSphere: ;
        else
        begin
        // fCubeFrame.Visible:=false;
        end;

      end;
      fGlobalFrame.SetCurrentObject(item);
      btnDelete.Visible:=tvItems.Selected <>tvItems.Items[0];
    end;
  begin
    if tvItems.Selected<>nil then
    begin
      fScene.ObjectSelect(TKLNObject(tvItems.Selected.TagObject),true);
      FillObjectProperties();
    end;
  end;

  procedure TFrmMain.tvItemsDragChange(SourceItem, DestItem: TTreeViewItem;
    var Allow: Boolean);
  begin
    if SourceItem=nil then
      exit;
    if SourceItem=tvItems.Items[0] then
      exit;

    if DestItem<>nil then
      TKLNObject(SourceItem.TagObject).SetParent(TKLNObject(DestItem.TagObject))
    else
      TKLNObject(SourceItem.TagObject).SetParent(fCurProject);
  end;

procedure TFrmMain.tvItemsDragOver(Sender: TObject; const Data: TDragObject;
  const Point: TPointF; var Operation: TDragOperation);
var
  it:TTreeViewItem;
begin
   it:= tvItems.ItemByPoint(Point.X,point.Y);
   if it<>nil then
    if it<>tvItems.Selected then
      if tvItems.Selected.IsChild(it) then
        abort;
end;

{$ENDREGION}

{$REGION 'Form Events'}


procedure TFrmMain.FormCreate(Sender: TObject);
  begin
    fScene:= TFMXScene.create(self);
    fGlobalFrame:=TFramePropGlobal.Create(self,fScene,procedure
    begin
      if (tvItems.Selected<>nil) and (tvItems.Selected.TagObject<>nil) then
        tvItems.Selected.Text:=TKLNObject(tvItems.Selected.TagObject).Name;
    end);
    fGlobalFrame.Visible:=False;
    fGlobalFrame.Align:=TAlignLayout.Top;
    fGlobalFrame.Parent:=PProps;

//    fCubeFrame:=TFramePropCube.Create(self,fScene,procedure
//    begin
//      if (tvItems.Selected<>nil) and (tvItems.Selected.TagObject<>nil) then
//        tvItems.Selected.Text:=TKLNObject(tvItems.Selected.TagObject).Name;
//    end);
//    fCubeFrame.Visible:=False;
//    fCubeFrame.Align:=TAlignLayout.Client;
//    fCubeFrame.Parent:=PProps;
    //New project - Refactor
    tvItems.Clear;
    fCurProject:=fScene.ProjectCreate('New');
    TreeAddItem(fCurProject,false);
  end;

  procedure TFrmMain.FormDestroy(Sender: TObject);
  begin
    if fGlobalFrame<>nil then
      fGlobalFrame.destroy();
    if fCubeFrame<>nil then
      fCubeFrame.destroy();
    fScene:=nil;
  end;


{$ENDREGION}

end.
